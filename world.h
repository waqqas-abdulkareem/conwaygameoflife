#ifndef WORLD_H
#define WORLD_H

#include <vector>
#include <iostream>
#include "point8.h"

namespace GameOfLife{
class World
{
    const static unsigned char CreationThreshold = 3;
    const static unsigned char SustaniabilityThreshold = 2;

    int _width;
    bool* _life;

    void clear();
    void seed(const std::vector<Point8>& lifeSites);
    bool lifeCanSurviveAt(const Point8& point) const;
    int surroundingLifeCount(const Point8& point) const;
    bool hasLife(const Point8& point) const;
    bool hasLife(int index) const;
    int size() const;
    int width() const;
    int indexForPoint(const Point8&) const;

public:

    World(int width, const std::vector<Point8>& premordialLifeSites){
        _width = width;
        _life = new bool[_width * _width];

        seed(premordialLifeSites);
    }

    ~World(){
        delete[] _life; _life = nullptr;
    }

    void evolve();
    int totalLifeCount() const;

    friend std::ostream& operator<<(std::ostream& ostream, const World& world);

};
}

#endif // WORLD_H
