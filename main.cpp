#include <iostream>
#include <vector>
#include <thread>
#include <chrono>
#include <sstream>
#include <algorithm>
#include "cmdline.h"

#include "world.h"
#include "point8.h"

#define DefaultWorldWidth 10
#define DefaultNumberIterations 10

using namespace std;

struct Arguments {
    int width;
    int iterations;
    vector<GameOfLife::Point8> seeds;
};

vector<GameOfLife::Point8> parseSeedsArgument(int width, const std::string& seedsArgument)
{
    std::istringstream seedsStream{ seedsArgument };
    std::string pointString;
    std::vector<GameOfLife::Point8> seeds;

    while (std::getline(seedsStream, pointString, ':')) {

        replace(pointString.begin(), pointString.end(), ',', ' ');
        istringstream pointStream{ pointString };

        int y;
        pointStream >> y;
        int x;
        pointStream >> x;

        if (y >= width || x >= width) {
            std::cerr << "Ignoring point y: " << y << ", x: " << x << " because it exceeds width: " << width << std::endl;
            continue;
        }

        seeds.push_back({ static_cast<unsigned char>(y), static_cast<unsigned char>(x) });
    }

    return seeds;
}

void parseArguments(int argc, char** argv, Arguments& args)
{
    cmdline::parser parser;

    parser.add<int>("width", 'w', "World Width", false,
        DefaultWorldWidth, cmdline::range(1, 30));

    parser.add<int>("iterations", 'i', "Number of Iterations", false,
        DefaultNumberIterations, cmdline::range(0, 100));

    parser.add<std::string>("seeds", 's',
        "Premordial life coordinates expressed y1,x1:y2,x2:y3,x3",
        true, "");

    parser.parse_check(argc, argv);

    args.width = parser.get<int>("width");
    args.iterations = parser.get<int>("iterations");
    std::string seeds = parser.get<std::string>("seeds");
    args.seeds = parseSeedsArgument(args.width, seeds);
}

int main(int argc, char** argv)
{
    Arguments args;
    parseArguments(argc, argv, args);

    GameOfLife::World world{ args.width, args.seeds };
    
    for (int i = 0; world.totalLifeCount() > 0 && i <= args.iterations; i++) {
        system("clear");
        std::cout << world << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        world.evolve();
    }
}