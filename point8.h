#ifndef POINT8_H
#define POINT8_H

#include <iostream>

namespace GameOfLife{
class Point8
{

public:
    Point8(unsigned char y, unsigned char x):y{y},x{x}{}
    Point8(const Point8& point):y{point.y},x{point.x}{}
    Point8(Point8&& point):y{std::move(point.y)},x{std::move(point.x)}{}
    ~Point8(){}

    const unsigned char y;
    const unsigned char x;

    friend std::ostream& operator<<(std::ostream& ostream, const Point8& point);
};
}

#endif // POINT8_H
