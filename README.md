# Conway Game Of Life

My implementation of [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) in C++11.

## Usage

`./ConwayGameOfLife --seeds=string [options]`

##### Options
**-s | --seeds** (Required): A list of points on the World where the initial seeds of life are located. Points must be expressed in the following format: `y1,x1:y2,x2:y3,x3` e.t.c (See Examples below).
Any point that is outside of the width of the world will be ignored.

**-w | -width** (Optional): Conway's Game of Life represents the World as a grid. Width is the measure of one side of the grid. The default value is 10.

**-i | --iterations** (Optional): Number of times the evolution process should run.

##### Examples

`./ConwayGameOfLife -w 6 -i 10 -s 1,2:1,3:1,4:2,1:2,2:2,3`

![alt tag](https://upload.wikimedia.org/wikipedia/commons/1/12/Game_of_life_toad.gif)

`./ConwayGameOfLife -w 16 -i 10 -s 2,4:2,5:2,6:2,10:2,11:2,12:\
4,2:4,7:4,9:4,14:\
5,2:5,7:5,9:5,14:\
6,2:6,7:6,9:6,14:\
7,4:7,5:7,6:7,10:7,11:7,12:\
9,4:9,5:9,6:9,10:9,11:9,12:\
10,2:10,7:10,9:10,14:\
11,2:11,7:11,9:11,14:\
12,2:12,7:12,9:12,14:\
14,4:14,5:14,6:14,10:14,11:14,12`

![alt tag](https://upload.wikimedia.org/wikipedia/commons/0/07/Game_of_life_pulsar.gif)

## Libraries

- [cmdline](https://github.com/tanakh/cmdline.git): A really straight-forward command line parser for C++, based on STL.