#include "world.h"

namespace GameOfLife {

std::ostream& operator<<(std::ostream& ostream, const World& world)
{
    for (unsigned char y = 0; y < world.width(); y++) {
        for (unsigned char x = 0; x < world.width(); x++) {
            ostream << (world._life[world.indexForPoint({ y, x })] ? "X" : "_");
        }

        ostream << std::endl;
    }

    return ostream;
}

bool World::lifeCanSurviveAt(const Point8& point) const
{
    const int surroundingLife = surroundingLifeCount(point);
    bool alive = hasLife(point);

    if (!alive && surroundingLife == CreationThreshold) {
        return true;
    }

    return alive && (surroundingLife == CreationThreshold || surroundingLife == SustaniabilityThreshold);
}

void World::evolve()
{
    std::vector<Point8> sites;
    for (unsigned char y = 0; y < width(); y++) {
        for (unsigned char x = 0; x < width(); x++) {
            Point8 p{ y, x };
            if (lifeCanSurviveAt(p)) {
                sites.push_back(std::move(p));
            }
        }
    }
    seed(std::move(sites));
}

int World::surroundingLifeCount(const Point8& point) const
{
    int lifeCount = 0;
    int index = indexForPoint(point);

    lifeCount += hasLife(index - width() - 1);
    lifeCount += hasLife(index - width());
    lifeCount += hasLife(index - width() + 1);

    lifeCount += hasLife(index - 1);
    lifeCount += hasLife(index + 1);

    lifeCount += hasLife(index + width() - 1);
    lifeCount += hasLife(index + width());
    lifeCount += hasLife(index + width() + 1);

    return lifeCount;
}

int World::totalLifeCount() const
{
    int lifeCount = 0;
    
    for (int i = 0; i < size(); ++i) {
        if (_life[i]) {
            ++lifeCount;
        }
    }

    return lifeCount;
}

bool World::hasLife(const Point8& point) const
{
    return hasLife(indexForPoint(point));
}

bool World::hasLife(int index) const
{
    if (index >= 0 && index < size()) {
        return _life[index];
    }
    return false;
}

int World::size() const
{
    return width() * width();
}

int World::width() const
{
    return _width;
}

void World::clear()
{
    memset(_life, 0, size() * sizeof(bool));
}

void World::seed(const std::vector<Point8>& lifeSites)
{
    clear();
    for (unsigned long i = 0; i < lifeSites.size(); i++) {
        _life[indexForPoint(lifeSites[i])] = true;
    }
}

int World::indexForPoint(const Point8& point) const
{
    return point.y * width() + point.x;
}
}
