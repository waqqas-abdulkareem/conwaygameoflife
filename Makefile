#------------------------------------------------------------------------------

SOURCE=point8.cpp world.cpp main.cpp
OUTPUT=ConwayGameOfLife
LIBRARIES=
INCLUDES=-I../cmdline/
CC=clang++
CCFLAGS=-stdlib=libc++ -std=c++11 -Wall -Wunused -fsanitize=address

#------------------------------------------------------------------------------

all: $(OUTPUT)

$(OUTPUT): format $(SOURCE)

	$(CC) $(CCFLAGS) $(INCLUDES) $(SOURCE) -o$(OUTPUT) $(LIBRARIES)

clean:
	rm -f $(OUTPUT)

format:
	clang-format -i -style=WebKit $(SOURCE)