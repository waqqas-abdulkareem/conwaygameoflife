#include "point8.h"

namespace GameOfLife {
std::ostream& operator<<(std::ostream& ostream, const Point8& point)
{
    return ostream << "Point8{y: " << point.y << ", x: " << point.x << " }";
}
}
